import { createRouter, createWebHistory } from 'vue-router';

const routerHistory = createWebHistory();

const routes: Array<any> = [
    {
        path: '/api/doc/',
        name: 'page',
        component: () => import('@doc/components/page.module.vue')
    }
];

const router: any = createRouter({
    history: routerHistory,
    routes
})

export default router;