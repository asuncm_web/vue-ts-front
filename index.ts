import { createApp } from 'vue';

import App from './app.module.vue';
import router from './app.router';

const app = createApp(App as any);

app.use(router);

app.mount('#root');