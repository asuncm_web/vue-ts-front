interface Host {
    pathname: string,
    reg: any
}

export const hostInfo: Host = {
    pathname: '/api/doc/',
    reg: /^\/api\/doc\//
}